public class Customer {

    private final int customerId;
    private final String name;
    private final String email;
    private final String phoneNumber;
    private double balanceLimits;

    public Customer(int customerId, String name, String email, String phoneNumber, double balance) {
        this.customerId = customerId;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.balanceLimits = balance;
    }

    public int getCustomerId() {return customerId;}

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {return phoneNumber;}

    public double getBalance() {
        return balanceLimits;
    }

    public void setBalanceLimits(double balanceLimits) {this.balanceLimits = balanceLimits;}

    public double getBalanceLimits() {return balanceLimits;}

    @Override
    public String toString() {
        return "ID: " + customerId +
                " | name = '" + name + '\'' +
                " | email = '" + email + '\'' +
                " | phoneNumber = '" + phoneNumber + '\'' +
                " | balance = " + balanceLimits;
    }
}

