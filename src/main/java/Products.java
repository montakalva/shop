public class Products {

    private final int id;
    private final String name;
    private int amount;
    private double price;
    private final String measurement;
    private final String color;
    private int onSale;

    public Products(int id, String name, int amount, double price, String measurement, String color, int onSale) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.measurement = measurement;
        this.color = color;
        this.onSale = onSale;
    }

    public Products(int id, int amount) {
        this.id = id;
        this.amount = amount;
        this.color = getColor();
        this.measurement = getMeasurement();
        this.name = getName();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getMeasurement() {
        return measurement;
    }

    public String getColor() {
        return color;
    }

    public int getOnSale() {
        return onSale;
    }

    public void setAmount(int amount) {this.amount = amount;}

    public void setOnSale(int onSale) {
        this.onSale = onSale;
    }

    @Override
    public String toString() {
        return "Available products: ID "+ id + " | " +
                name + " | " + color  + " | " +
                " with price " + price + " | " +
                " amount: " + amount + " " + measurement;
    }
}
