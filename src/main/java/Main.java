import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        SalesProcess shop = new SalesProcess();
        shop.exitingProducts();
        shop.existingCustomers();

       JOptionPane.showMessageDialog( null, "Welcome to shop!");

        boolean quite = false;
        String choice;
        printInstructions();
        while (!quite){
            String [] choiceList = {"Print the list of products",
                                    "Create new Account",
                                    "Add a product to shopping cart",
                                    "Products on Sale",
                                    "My shopping cart",
                                    "Payment",
                                    "Print instructions",
                                    "To add new product",
                                    "Print Existing Customers",
                                    "To quit"};
          choice = (String) JOptionPane.showInputDialog(
                  null,
                  "Enter your choice: ",
                  "Chose option",
                  JOptionPane.QUESTION_MESSAGE,
                  null,
                  choiceList,
                  choiceList[0]
          );

        switch (choice){
            case "Print the list of products":
                shop.printProductList();
                break;
            case "Create new Account":
                shop.createNewCustomer();
                break;
            case "Add a product to shopping cart":
                shop.addShoppingCartList();
                break;
            case "Products on Sale":
                shop.printProductsOnSale();
                break;
            case "My shopping cart":
                shop.PrintMyShoppingCart();
                break;
            case "Payment":
                shop.Payment();
                break;
            case "Print instructions":
                printInstructions();
                break;
            case "To add new product":
                shop.addProductList();
                break;
            case "Print Existing Customers":
                shop.printExistingCustomers();
                break;
            case "To quit":
                quite = true;
                break;
            default:
                System.out.println("Something went wrong, try again or contact us: support@shop.com; +371 988 988 98");
            }
        }
    }
    public static void printInstructions(){
        System.out.println("\nPress");
        System.out.println("\t Print the list of products");
        System.out.println("\t Create new Account");
        System.out.println("\t Add a product to shopping cart");
        System.out.println("\t Products on Sale");
        System.out.println("\t My shopping cart");
        System.out.println("\t Payment");
        System.out.println("\t Print instructions");
        System.out.println("\t To add new product");
        System.out.println("\t Print Existing Customers");
        System.out.println("\t To quit");
    }
}
