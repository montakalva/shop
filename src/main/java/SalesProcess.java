import javax.swing.*;
import java.util.ArrayList;

public class SalesProcess {

    int availability;
    int productToBuy;
    int amountToBuy;
    double totalPrice;
    int customerId;

    private ArrayList<Products> shoppingCartList = new ArrayList<>();
    private static ArrayList<Customer> customers = new ArrayList<>();
    private static ArrayList<Products> productList = new ArrayList<>();

   public void existingCustomers(){
        Customer customer1 = new Customer(customers.size(), "John", "johndo@gmail.com", "+371 23232323", 50.00D);
        this.customers.add(customer1);
        Customer customer2 = new Customer(customers.size(), "Sara", "sarablack@gmail.com", "+371 262626260", 100.0D );
        this.customers.add(customer2);
    }

    public void createNewCustomer() {
        int customerId = customers.size();
        String name = JOptionPane.showInputDialog("Insert your Name Surname");
        String email = JOptionPane.showInputDialog(name + ", insert your contact e-mail!");
        String phoneNumber = JOptionPane.showInputDialog("Insert your contact phone number");
        double balance = Double.parseDouble(JOptionPane.showInputDialog("Enter your balance"));

        Customer customer = new Customer(customerId, name, email, phoneNumber, balance);
        this.customers.add(customer);
        JOptionPane.showMessageDialog(null, "Customer " + customer.getName() + " added successfully!");

        System.out.println(customer + " Customer added successfully!");
    }

    public String printExistingCustomers(){
        System.out.println("Customer list: ");
        for (Customer existingCustomers : customers){
            System.out.println(existingCustomers);
        }
        return "Sorry, there are no current customers to show!";
    }

    public void exitingProducts() {
        Products rosesWhite = new Products(productList.size(), "Roses", 80, 2.15D, "pcs", "White", 15);
        this.productList.add(rosesWhite);
        Products rosesPink = new Products(productList.size(), "Roses", 100, 2.00D, "pcs", "Pink", 0);
        this.productList.add(rosesPink);
        Products rosesRed = new Products(productList.size(), "Roses", 120, 2.5D, "pcs", "Red", 0);
        Products tulipsPink = new Products(productList.size(), "Tulips", 40, 0.75D, "pcs", "Pink", 0);
        this.productList.add(tulipsPink);
        Products tulipsRed = new Products(productList.size(), "Tulips", 40, 0.75D, "pcs", "Red", 30);
        this.productList.add(tulipsRed);
        Products liliesYellow = new Products(productList.size(), "Lilies", 20, 4.5D, "pcs", "Yellow", 0);
        this.productList.add(liliesYellow);
        Products peoniesWhite = new Products(productList.size(), "Peonies", 10, 2.75D, "pcs", "White", 70);
        this.productList.add(peoniesWhite);
        Products peoniesYellow = new Products(productList.size(), "Peonies", 10, 2.75D, "pcs", "Yellow", 0);
        this.productList.add(peoniesYellow);
    }

    public void addProductList() {

        int id = productList.size();
        String name = JOptionPane.showInputDialog("Enter product name");
        int amount = Integer.parseInt(JOptionPane.showInputDialog("Enter product amount"));
        double price = Double.parseDouble(JOptionPane.showInputDialog("Enter price of product"));

        String [] availableMeasurements = {"Pcs", "Item"};

        String measurement = (String) JOptionPane.showInputDialog(
                null,
                "Enter product measurement",
                "Product Measurement",
                JOptionPane.QUESTION_MESSAGE,
                null,
                availableMeasurements,
                availableMeasurements[0]
        );

        String[] colorOptions = {"White", "Pink", "Red", "Yellow"};
        String color = (String) JOptionPane.showInputDialog(
                null,
                "Select flowers color",
                "Flowers color",
                JOptionPane.QUESTION_MESSAGE,
                null,
                colorOptions,
                colorOptions[0]
        );
        int onSale = Integer.parseInt(JOptionPane.showInputDialog("Enter product discount amount in percentages"));

        Products products = new Products(id, name, amount, price, measurement, color, onSale);
        this.productList.add(products);
        JOptionPane.showMessageDialog(null, "Product " + products.getName() + " added successfully!");

        System.out.println(products + "You successfully added products ");
    }

    public void  printProductList() {
        System.out.println("Available product list:");
        for(Products productsToShow : productList){
            System.out.println(productsToShow);
        }
    }

    public void addShoppingCartList() {
        if(this.shoppingCartList.size() == 0) {
            productToBuy = Integer.parseInt(JOptionPane.showInputDialog("Enter product ID"));
            amountToBuy = Integer.parseInt(JOptionPane.showInputDialog("Enter product amount"));

            Products productsSelected = new Products(productToBuy, amountToBuy);

            availableAmount(productToBuy, amountToBuy);
            if (availability == 1) {
                this.shoppingCartList.add(productsSelected);
                System.out.println("You successfully added products");
            } else if (availability == 0){
                System.out.println("Product not available in necessary amount!");
            }
        } else {
            System.out.println( "You have product in shopping cart, please make payment");
        }
   }

    public void Payment(){

        totalPrice = priceForPurchase(productToBuy, amountToBuy);
        System.out.printf("Purchase total cost: %.2f EUR\n", totalPrice);
        availability = availableAmount(productToBuy,amountToBuy);
        if (availability == 0){
            System.out.println("Sorry, product not available at necessary amount.");
        }
        customerId = Integer.parseInt(JOptionPane.showInputDialog("Enter your customer ID number: "));
        int balanceCheck = checkBalance(customerId, totalPrice);
        if (balanceCheck == 1){
            reduceProductAmount(productToBuy, amountToBuy);
            reduceBalance(customerId, totalPrice);
            shoppingCartList.clear();

            System.out.println("Payment was successful!");
        }
        if (balanceCheck == 0){
            System.out.println("Not enough money on your account!");
        }
    }

    public int checkBalance(int customerId, double totalPrice){
        int temporary = 0;
        for (Customer customer: customers) {
            if (customer.getCustomerId()==(customerId)) {
                if (customer.getBalance()>=totalPrice){
                    temporary = 1;
                    break;
                }
            }
        }
        return temporary;
    }

    public void reduceBalance(int customerId, double totalPrice) {
        for (Customer customer : customers) {
            if (customer.getCustomerId() == customerId) {
                double currentBalance= customer.getBalance() - totalPrice;
                customer.setBalanceLimits(currentBalance);
                break;
            }
        }
    }

    public int availableAmount(int productToBuy, int amountToBuy){
        availability = 0;
        Products selectedProduct = productList.get(productToBuy);
        if (selectedProduct.getAmount() >= amountToBuy){
            availability = 1;
        }
        return availability;
    }

   public void reduceProductAmount(int productToBuy, int amountToBuy){
        for (Products prod: productList) {
            if (prod.getId() == productToBuy) {
                int currentAmount = prod.getAmount() - amountToBuy;
                prod.setAmount(currentAmount);
                break;
            }
        }
    }

    public static double priceForPurchase(int productToBuy, int amountToBuy){
        Products selectedProduct = productList.get(productToBuy);
        double totalPrice = ((selectedProduct.getPrice() - ((selectedProduct.getPrice() / 100) * selectedProduct.getOnSale())) * amountToBuy);
        return totalPrice;
    }

    public String PrintMyShoppingCart(){
        System.out.println("Your ShoppingCart: ");
        for (Products myShoppingCart : shoppingCartList){
            System.out.println(myShoppingCart);
        }
        return "Sorry, there are no products to show!";
    }

    public String printProductsOnSale(){
        System.out.println("Product on Sale! Don't miss your opportunity!");
        for (Products productsOnSale: productList){
            if (productsOnSale.getOnSale() > 0){
                double discountPrice = (productsOnSale.getPrice() - ((productsOnSale.getPrice() / 100) * productsOnSale.getOnSale()));
                System.out.printf("ID: %d | %s |  %s | with price %.2f | available with new price: %.2f | Available amount: %d \n",
                        productsOnSale.getId(), productsOnSale.getName(),productsOnSale.getColor(), productsOnSale.getPrice(), discountPrice, productsOnSale.getAmount());
            }
        }
        return "Sorry, we don't have products on sale";
    }
}




